import { DataTypes, Model, Sequelize } from 'sequelize';
import User from './User';

interface AddressAttributes {
  userId: number;
  address: string;
}

// You can write `extends Model<AddressAttributes, AddressAttributes>` instead,
// but that will do the exact same thing as below
export default class Address
  extends Model<AddressAttributes>
  implements AddressAttributes
{
  public userId!: number;
  public address!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export function initAddress(sequelize: Sequelize) {
  Address.init(
    {
      userId: {
        type: DataTypes.INTEGER
      },
      address: {
        type: new DataTypes.STRING(128),
        allowNull: false
      }
    },
    {
      tableName: 'address',
      sequelize // passing the `sequelize` instance is required
    }
  );
}
