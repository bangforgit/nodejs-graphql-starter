import { Sequelize } from 'sequelize';
import dotenv from 'dotenv';
import User from './User';
import TableInit from './TableInit';
dotenv.config();

function getSequelize() {
  const sequelize = new Sequelize(
    process.env.DB_HOST || 'postgres://postgres:123456@localhost:5432/postgres'
  );
  return sequelize;
}

const sequelize = getSequelize();
TableInit(sequelize);

async function doStuffWithUser() {
  const newUser = await User.create({
    name: 'Johnny',
    preferredName: 'John'
  });
  console.log(newUser.id, newUser.name, newUser.preferredName);

  const project = await newUser.createProject({
    name: 'first!'
  });

  const ourUser = await User.findByPk(5, {
    include: [User.associations.projects],
    rejectOnEmpty: true // Specifying true here removes `null` from the return type!
  });
  await ourUser.destroy();
}
doStuffWithUser().then(() => {});
