import { Optional, Model, DataTypes, Sequelize } from 'sequelize';

interface ProjectAttributes {
  id: number;
  ownerId: number;
  name: string;
}

interface ProjectCreationAttributes extends Optional<ProjectAttributes, 'id'> {}

export default class Project
  extends Model<ProjectAttributes, ProjectCreationAttributes>
  implements ProjectAttributes
{
  public id!: number;
  public ownerId!: number;
  public name!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export function initProject(sequelize: Sequelize) {
  Project.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      ownerId: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      name: {
        type: new DataTypes.STRING(128),
        allowNull: false
      }
    },
    {
      sequelize,
      tableName: 'projects'
    }
  );
}
