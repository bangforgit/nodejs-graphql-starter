import { Sequelize } from 'sequelize';

import Address, { initAddress } from './Address';
import { initNote } from './Note';
import Project, { initProject } from './Project';
import User, { initUser } from './User';

export default function TableInit(sequelize: Sequelize) {
  initUser(sequelize);
  initAddress(sequelize);
  initProject(sequelize);

  initNote(sequelize);

  User.hasMany(Project, {
    sourceKey: 'id',
    foreignKey: 'ownerId',
    as: 'projects' // this determines the name in `associations`!
  });
  Address.belongsTo(User, { targetKey: 'id' });
  User.hasOne(Address, { sourceKey: 'id' });
}
