import { Optional, Model, Sequelize, DataTypes } from 'sequelize';

// You can also define modules in a functional way
interface NoteAttributes {
  id: number;
  title: string;
  content: string;
}

// You can also set multiple attributes optional at once
interface NoteCreationAttributes
  extends Optional<NoteAttributes, 'id' | 'title'> {}

// And with a functional approach defining a module looks like this

export default class Note
  extends Model<NoteAttributes, NoteCreationAttributes>
  implements NoteAttributes
{
  public id!: number;
  public title!: string;
  public content!: string;
}

export function initNote(sequelize: Sequelize) {
  Note.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      title: {
        type: new DataTypes.STRING(64),
        defaultValue: 'Unnamed Note'
      },
      content: {
        type: new DataTypes.STRING(4096),
        allowNull: false
      }
    },
    {
      tableName: 'notes',
      sequelize // passing the `sequelize` instance is required
    }
  );
}
