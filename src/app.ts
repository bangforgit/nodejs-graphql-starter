import express from 'express';
import { ApolloServer, gql, IResolvers } from 'apollo-server-express';
import dotenv from 'dotenv';
dotenv.config();

const app = express();
const port = process.env.PORT || 4000;

const typeDefs = gql`
  type Query {
    hello: String
  }
`;
const resolvers = {
  Query: {
    hello: () => 'Hello World!'
  }
};
const server = new ApolloServer({
  typeDefs,
  resolvers
});

server.applyMiddleware({ app, path: '/api' });

app.listen({ port }, () => {
  console.log(
    `server is listening on http://localhost:${port}${server.graphqlPath}`
  );
});
